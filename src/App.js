import "./App.css";

function App() {
  return (
    <div className="h-[100vh]">
      <div className='bg-gradient-to-tr from-purple-600 to-green-700 h-full w-full  relative'>
      <img src="https://images.pexels.com/photos/2693212/pexels-photo-2693212.png?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2" alt="pexels 2693212" className="w-full h-full object-cover absolute mix-blend-color-burn"/>
      <div className="p-24">
        <h1  className="text-green-400 text-6xl font-bold">This is the Headline </h1>
        <h2 className="text-blue-300 text-3xl font-light mt-5">Some really great stuff goes here</h2>
        </div></div>
    </div>
  );
}

export default App;
